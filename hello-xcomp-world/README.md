Hello Cross Compiler World
==========================

This project demonstrates minimal cross compilation using gcc and mingw on
Linux to cross compile a console app that will run on Linux or Windows / Wine.

For some complex projects purpose built cross complier(s) may be preferable.
This project serves as a small first step into the world of cross compilation.

More details are available in the [Hello Cross Compiler World.](http://codelobe.com/dev/hello-cross-compiler-world)

Quick & Dirty Guide
-------------------

The `Makefile` demonstrates building for multiple target platforms.
Run `make` with no parameters to see the build target options.


### With Vagrant Do ###

1. Optional: Edit the Vagrantfile to select a different VM box.
2. `vagrant up`
3. `vagrant ssh`
4. `cd /vagrant`
5. `make all`
6. Run the compiled binaries.

Vagrant can be used to compile the project on non-Linux platforms.

See the [Vagrant Introduction article](http://codelobe.com/dev/vagrant-intro)
for Vagrant installation & use.


### With a GNU/Linux OS ###

1. `apt install build-essential make mingw-w64 g++-multilib`
2. `make all`
3. Run the compiled binaries, possibly via `wine` or `wineconsole cmd`

Use WINE to run the Windows binaries on Linux, e.g.:
```
wine hello-world-win32 [parameter list]
```
  or
```
wineconsole cmd
Z:\...>hello-world-win64 [parameter list]
```

#include <stdio.h>

/// Echo greeting and parameter list to standard output.
int main( int argc, char ** args )
{	puts( "Hello cross complier world!" );
	int param = 0;
	for ( ; param < argc; ++param )
	{	printf( "Parameter #%i = [%s]\n", param, args[ param ] );
	}
	return 0;
}

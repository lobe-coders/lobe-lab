Intro to Vagrant VM Development
===============================

The Vagrantfile accompanies [this CodeLobe Article.](http://codelobe.com/dev/vagrant-intro)

Quick & Dirty Guide
-------------------

1. Install [Virtual Box](https://www.virtualbox.org/)
2. Install [Vagrant](https://www.vagrantup.com/downloads.html)
3. Copy the Vagrantfile to a new or empty directory.
4. Open a terminal
5. Set current working directory: `cd [/home/user/yourNewDir]`
6. Execute `vagrant up`

See the article linked above and read the [Vagrantfile](http://codelobe.com/dev/vagrant/Vagrantfile) for more details.
